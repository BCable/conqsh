<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<!--
/*
 * Conqsh v0.1pa
 *
 * Author: Brad Cable
 * Email: brad@bcable.net
 * License: MIT
 *
 */
-->

<html>
<head>
	<title>Conqsh</title>
	<link rel="stylesheet" type="text/css" href="main.css" />
	<script language="javascript" type="text/javascript">
	<!--

		// includes Javascript files
		var include=function(jsfile){
			var head=document.getElementsByTagName('head')[0];
			var script=document.createElement('script');
			script.setAttribute('language','javascript');
			script.setAttribute('type','text/javascript');
			script.setAttribute('src',jsfile);
			head.appendChild(script);
		}

		// includes a CSS file
		var cinclude=function(cssfile){
			var head=document.getElementsByTagName('head')[0];
			var link=document.createElement('link');
			link.setAttribute('rel','stylesheet');
			link.setAttribute('type','text/css');
			link.setAttribute('href',cssfile);
			head.appendChild(link);
		}

		// includes a Javascript file in relation to the current module
		var mod_include=function(jsfile){
			include('modules/'+modules.currentModuleName+'/'+jsfile);
		}

		// includes a CSS file in relation to the current module
		var mod_cinclude=function(cssfile){
			cinclude('modules/'+modules.currentModuleName+'/'+cssfile);
		}

		// ends a module
		var mod_end=function(){
			modules.completeLoad();
		}

		// server_interface variable
		include('interface.js');

		// object expanding
		include('core/string.js');

		// helper classes/objects
		include('core/code.js');
		include('core/awindow.js');
		include('core/menu.js');
		include('core/enc.js');
		include('core/server.js');
		include('core/time.js');

		// command stuff
		include('core/command.js');
		include('core/cmd.js');
		include('core/alias.js');
		include('core/win.js');

		// dialog stuff
		include('core/darkener.js');
		include('core/dialog.js');

		// users and settings stuff
		include('core/user.js');
		include('core/settings.js');

		// modules
		include('installedModules.js');
		include('core/modules.js');

		// stuff that must be executed last
		include('core/rootmenu.js');
		include('core/localmenu.js');
		include('core/core.js');
	//-->
	</script>
</head>
<body>
	<script language="javascript" type="text/javascript">
	<!--
		core.init();
	//-->
	</script>
</body>
</html>
