/*
 * Conqsh - server_interface variable
 *
 * Stores a link to the main backend PHP file.  This is needed in multiple
 * locations, which is why it has a separate file.
 *
 * Author: Brad Cable
 * Email: brad@bcable.net
 * License: Modified BSD
 * License Details:
 * http://bcable.net/license.php
 *
 */

var server_interface="backend/interface.php";
