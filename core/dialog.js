/*
 * Conqsh - dialog class
 *
 * Displays a dialog to be displayed to the user.
 *
 * Author: Brad Cable
 * Email: brad@bcable.net
 * License: MIT
 *
 */

// constructor
function dialog(title){
	// title to display for the dialog box
	this.title=title;

	// automatically destroys the window after everything is handled
	this.autoDestroy=true;

	// stores the error to be displayed, if null, nothing
	this.errorText=null;

	// the type of dialog box
	if(arguments.length==1) this.type=dialog.prototype.TYPE_SUBMIT;
	else this.type=arguments[1];

	// list of captions for buttons
	this.buttons=Array('Okay', 'Cancel');

	// element number (array form) that gets focus initially
	this.firstFocus=0;

	// internal - don't touch even with external code plzkthx

	// keeps track of whether or not the window is being shown
	this.shown=false;

	// keeps track of the table rows to be displayed in the future
	this.tablerows=Array();

	// keeps track of whether or not the form has been submitted once or not
	this.submittedOnce=false;

	// create the block element
	this.blk=code.mkc(null,'div','dialog');
}

dialog.prototype={

// dialog types
TYPE_SUBMIT: 1,
TYPE_OKAY_CANCEL: 2,

// sets autoDestroy
setAutoDestroy: function(bool){
	this.autoDestroy=bool;
},

// sets the error text
setError: function(error){
	this.errorText=error;
	if(this.errorField && this.errorSpacer)
		this.displayError();
},

// displays the errorText
displayError: function(){
	if(this.errorText==null){
		this.errorField.style.display='none';
		this.errorSpacer.style.display='none';
	}
	else{
		this.errorField.style.display='table-cell';
		this.errorSpacer.style.display='table-cell';
		this.errorField.innerHTML=this.errorText;
	}
},

// add a raw element to the dialog box
add: function(label,func){
	this.tablerows.push(Array(label,func));
},

// add a text field to the dialog box
addTextField: function(label,name){
	var value='';
	if(arguments.length!=2) value=arguments[2];

	this.add(label,function(parent){
		var elem=code.mk(parent,'input');
		elem.setAttribute('type','text');
		elem.setAttribute('name',name);
		elem.setAttribute('value',value);
		return elem;
	});
},

// add a password field to the dialog box
addPassField: function(label,name){
	var value='';
	if(arguments.length!=2) value=arguments[2];

	this.add(label,function(parent){
		var elem=code.mk(parent,'input');
		elem.setAttribute('type','password');
		elem.setAttribute('name',name);
		elem.setAttribute('value',value);
		return elem;
	});
},

// adds a flat text description in both columns of the table
addText: function(text){
	this.tablerows.push(Array(text));
},

// create the dialog box
create: function(){
	// create form and onsubmit handler
	this.form=code.mk(this.blk,'form');
	this.form.setAttribute('method','post');
	this.form.onsubmit=function(){
		this.parentDialog.submittedOnce=true;
		this.parentDialog.hide();

		if(this.parentDialog.submit)
			this.parentDialog.submit(this.parentDialog);

		if(this.parentDialog.autoDestroy && !this.parentDialog.shown)
			this.parentDialog.destroy();
		return false;
	}
	this.form.parentDialog=this;

	// create table
	this.tablelem=code.mkc(this.form,'table','dialog');
	this.tablelem.setAttribute('cellpadding',0);
	this.tablelem.setAttribute('cellspacing',0);

	// create title and spacer
	var titletr=code.mk(this.tablelem,'tr');
	var titletd=code.mkc(titletr,'td','dialogTitle');
	titletd.setAttribute('colspan',2);
	titletd.innerHTML=this.title;

	var spacetr=code.mkc(this.tablelem,'tr');
	code.mkc(spacetr,'td','dialogSpacer');

	// error spacer and error field
	var errtr=code.mkc(this.tablelem,'tr');
	this.errorField=code.mkc(errtr,'td','dialogError');
	this.errorField.setAttribute('colspan',2);
	var errspctr=code.mkc(this.tablelem,'tr');
	this.errorSpacer=code.mkc(errspctr,'td','dialogErrorSpacer');
	this.errorSpacer.setAttribute('colspan',2);
	this.displayError();

	// loop through the elements queued up and append them to the table
	for(var i=0; i<this.tablerows.length; i++){
		var tr=code.mk(this.tablelem,'tr');
		if(this.tablerows[i].length==1){
			var text=code.mkc(tr,'td','dialogText');
			text.setAttribute('colspan',2);
			text.innerHTML=this.tablerows[i][0];
		}
		else if(this.tablerows[i].length==2){
			var label=code.mkc(tr,'td','dialogLabel');
			label.innerHTML=this.tablerows[i][0];
			var item=code.mkc(tr,'td','dialogInput');
			this.tablerows[i][1](item);
		}
	}
	this.tablerows=Array();

	switch(this.type){

	// submit dialog box
	case dialog.prototype.TYPE_SUBMIT:
		var tr=code.mk(this.tablelem,'tr');
		var td=code.mkc(tr,'td','dialogSubmitRow');
		td.setAttribute('colspan',2);

		var submit=code.mk(td,'input');
		submit.setAttribute('type','submit');
		submit.setAttribute('value',this.buttons[0]);
		submit.style.width='100%';

		break;

	// okay/cancel dialog box
	case dialog.prototype.TYPE_OKAYCANCEL:
		var tr=code.mk(this.tablelem,'tr');
		var td=code.mkc(tr,'td','dialogSubmitRow');
		td.setAttribute('colspan',2);

		var ldiv=code.mk(td,'div');
		ldiv.style.cssFloat='left';
		var cancel=code.mk(ldiv,'input');
		cancel.setAttribute('type','button');
		cancel.setAttribute('value',this.buttons[1]);
		cancel.onclick=this.destroy;
		cancel.parentDialog=this;

		var rdiv=code.mk(td,'div');
		rdiv.style.cssFloat='right';
		var okay=code.mk(rdiv,'input');
		okay.setAttribute('type','submit');
		okay.setAttribute('value',this.buttons[0]);

		break;
	}
},

// centers the window on the screen
center: function(){
	this.blk.style.left=(innerWidth-this.blk.clientWidth)/2+'px';
	this.blk.style.top=(innerHeight-this.blk.clientHeight)/2+'px';
},

// show the dialog box
show: function(){
	this.blk.style.display='block';
	this.center();
	darkener.show();
	this.shown=true;

	// clear the form if necessary, then focus the primary field
	if(this.submittedOnce){
		for(var i=0; i<this.form.elements.length; i++){
			if(
				this.form.elements[i].type!='button' &&
				this.form.elements[i].type!='submit'
			) this.form.elements[i].value='';
		}
	}
	this.form.elements[this.firstFocus].focus();
},

// hide the dialog box
hide: function(){
	this.blk.style.display='none';
	this.shown=false;
	if(this.autoDestroy)
		darkener.hide();
},

// destroy the dialog box (can't show after this is called)
destroy: function(){
	var dialog=this;
	if(this.parentDialog) dialog=this.parentDialog;

	code.rm(dialog.blk);
	dialog.shown=false;
	darkener.hide();
},

}

// dialog.error is a function that displays the given text as an error
dialog.error=function(title,text){
	var dlog=new dialog((title!=null?"Error: "+title:"Error"));
	dlog.addText(text);
	dlog.create();
	dlog.show();
}
