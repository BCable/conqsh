/*
 * Conqsh - awindow class
 *
 * Describes and handles a single window.
 *
 * Author: Brad Cable
 * Email: brad@bcable.net
 * License: MIT
 *
 */

// constructor
function awindow(){
	// keeps track of the block element that is the body of the window
	this.blk=null;

	// keeps track of the title of the window
	this.title="";
}

// awindow class
awindow.prototype={

// login required for this class
loginRequired: false,

// mode associated with this class
mode: "",

// build window
build: function(){
	core.addWindow(this);
	core.selWin(core.windows.length-1);
},

// creates a new window class
new: function(){
	var ret=awindow;
	ret.prototype=code.ocpy(awindow.prototype);
	return ret;
},

// set the login required attribute
setLoginRequired: function(loginRequired){
	this.loginRequired=loginRequired;
},

// set the mode attribute
setMode: function(mode){
	this.mode=mode;
},

// set this specific window object's title
setTitle: function(title){
	this.title=title;
	core.redrawWinbar();
},

}
