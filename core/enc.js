/*
 * Conqsh - enc object
 *
 * Provides encryption and other security based functionality.
 *
 * Author: Brad Cable
 * Email: brad@bcable.net
 * License: MIT
 *
 */

enc={

version: 0,

// creates a hash of pw of length keylength
hash: function(pw,keylength){
	this.version=(arguments.length>2?arguments[2]:1);
	var key=new Array();
	if(keylength>pw.length){
		var charsper=parseInt(keylength/pw.length);
		for(var i=0; i<pw.length; i++){
			for(var j=0; j<charsper; j++){
				key.push(pw.charCodeAt(i));
			}
		}
	}
	else{
		var charsper=parseInt(pw.length/keylength);
		for(var i=0; i<pw.length; i+=charsper){
			var cumul=0;
			for(var j=i; j<pw.length && j<charsper; j++){
				cumul+=pw.charCodeAt(j);
			}
			key.push(cumul);
		}
	}

	if(key.length<keylength){
		for(var j=0; j<=keylength-key.length; j++){
			key.push(pw.charCodeAt(pw.length-1));
		}
	}

	for(var i=0; i<key.length-1; i++){
		var variance=i+this.expon(-1,i);
		key[i+1]+=key[i]+(i+2<key.length?key[i+2]:0)+variance;
		key[i+1]+=(variance<key.length?key[variance]:0);
		key[i+1]%=128;
	}
	for(var i=key.length-1; i>0; i--){
		var variance=i+this.expon(-1,i);
		key[i-1]+=key[i]+(i-2>=0?key[i-2]:0)+variance;
		key[i-1]+=(variance<key.length?key[variance]:0);
		key[i-1]%=128;
	}

	var thekey="";
	for(var i=0; i<key.length; i++){
		thekey+=String.fromCharCode(key[i]);
	}
	return thekey;
},

// a^b
expon: function(a,b){
	var num;
	if(b==0) return 1;
	num=a; b--;
	while(b>0){ num*=a; b--; }
	return num;
},

// converts ascii to a string of hex
hex: function(text){
	var hex="";
	var curhex;
	for(var i=0; i<text.length; i++){
		curhex=text.charCodeAt(i).toString(16);
		if(curhex.length==1) hex+="0";
		hex+=curhex;
	}
	return hex;
},

// converts string of hex to ascii
hextoascii: function(hex){
	var str="";
	for(var i=0; i<hex.length; i+=2){
		str+=String.fromCharCode(parseInt(hex.substr(i,2),16));
	}
	return str;
},

// encrypts text with the current user key
encrypt: function(text){
	if(!user.loggedIn){
		dialog.error(
			"Not Logged In",
			"You are not logged in to the system, and the action that was "+
			"just attempted requires you to be logged in."
		);
		return false;
	}

	for(var i=0; i<text.length; i++){
		text=
			text.substr(0,i)+
			String.fromCharCode(
				text.charCodeAt(i)+user.key.charCodeAt(i%user.key.length)
			)+
			text.substr(i+1,text.length);
	}

	return this.version+":"+enc.hex(text);
},

// decrypts text with the current user key
decrypt: function(text){
	var arr=text.split(":");
	if(arr[0]==1){
		text=this.hextoascii(arr[1]);
		for(var i=0; i<text.length; i++){
			text=
				text.substr(0,i)+
				String.fromCharCode(
					text.charCodeAt(i)-user.key.charCodeAt(i%user.key.length)
				)+
				text.substr(i+1,text.length);
		}
		return text;
	}
	else{
		dialog.error("Encryption Error","Unknown encryption format.");
		return false;
	}
},

}
