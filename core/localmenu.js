/*
 * Conqsh - localmenu object
 * instance of menu
 *
 * 'local' menu object controlled by the second button on the strip bar.
 *
 * Author: Brad Cable
 * Email: brad@bcable.net
 * License: MIT
 *
 */

localmenu=new menu('local_menu');

localmenu.menuItems=function(){
	win={}
	win.cur=function(){ obj={}; obj.mode='note'; return obj; }
	return eval("menu_mod_"+win.cur().mode+".menuItems");
}
