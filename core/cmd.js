/*
 * Conqsh - cmd object
 * instance of command
 *
 * Handle command execution.
 *
 * Author: Brad Cable
 * Email: brad@bcable.net
 * License: MIT
 *
 */

cmd=new command();

// can't address these directly because user.js is loaded after cmd.js
cmd.register('login',function(){ user.doLogin(); });
cmd.register('logout',function(){ user.doLogout(); });
cmd.register('close',function(){
	var winno=core.selected;
	if(winno>0){
		core.removeWindow(winno);
		core.selWin(winno-1);
		core.redrawWinbar();
	}
	// TODO: else, error
});

// registers the default command for the cmd object to look for aliases
cmd.register('',function(args){
	var cmdname=(args==undefined?'':args[0].toLowerCase());

	// check aliases
	if(alias.aliases[cmdname]){
		args.shift();
		cmd.exec(alias.aliases[cmdname],args);
	}
	else cmd.notfound();
});
