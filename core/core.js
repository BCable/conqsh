/*
 * Conqsh - core object
 *
 * Initializes and handles the entire app.
 *
 * Author: Brad Cable
 * Email: brad@bcable.net
 * License: MIT
 *
 */

core={

// keeps track of the windows associated with the winbar
windows: Array(),

// keeps track of the selected window
selected: null,

// initialize script
init: function(){
	core.loading();
	modules.loadModules();
	this.redraw();
	darkener.init();
	core.done();
},

// STATUS {{{

// running count of how many things are currently "loading"
loaders: 0,

// notes that one more thing is currently loading
loading: function(){
	var doinc=true;
	if(arguments.length!=0) doinc=arguments[0];
	if(doinc) this.loaders++;
	if(this.loaders>0 && this.root_button){
		this.root_button.style.backgroundColor="#800000";
	}
},

// removes one of the loaders from the list
done: function(){
	if(this.loaders>0){
		this.loaders--;
		if(this.loaders==0)
			this.root_button.style.backgroundColor="#FFFFFF";
	}
},

// }}}

// BUILD {{{

// redraws the entire screen
redraw: function(){
	core.loading();
	code.gebtn('body').innerHTML="";
	this.buildStripBar();
	this.buildCmdLine();
	this.buildBody();
	this.addRootWindow();
	core.done();
},

// builds the strip at the top of the screen
buildStripBar: function(){
	this.strip_bar=code.mk(null,'div','strip_bar');

	this.root_button=code.mk(this.strip_bar,'div','root_button');
	this.loading(false);
	var rb_a=code.mk(this.root_button,'a');
	rb_a.onclick=core.buildRootMenu;

	this.local_button=code.mk(this.strip_bar,'div','local_button');
	var lb_a=code.mk(this.local_button,'a');
	lb_a.onclick=core.buildLocalMenu;

	// time box
	this.time_box=code.mk(this.strip_bar,'div','time_box');
	time.handle(this.time_box);

	// window bar
	this.winbar_leftbtn=code.mk(this.strip_bar,'div','winbar_leftbtn');
	var wlb_a=code.mk(this.winbar_leftbtn,'a');
	wlb_a.innerHTML="&lt;";
	wlb_a.onmousedown=function(){ core.scrollWinbar(false); };
	wlb_a.onmouseup=this.cancelScrollWinbar;

	this.winbar_rightbtn=code.mk(this.strip_bar,'div','winbar_rightbtn');
	var wrb_a=code.mk(this.winbar_rightbtn,'a');
	wrb_a.innerHTML="&gt;";
	wrb_a.onmousedown=function(){ core.scrollWinbar(true); };
	wrb_a.onmouseup=this.cancelScrollWinbar;

	this.winbar=code.mk(this.strip_bar,'div','winbar');
},

// builds the command line bar
buildCmdLine: function(){
	this.cmdline=code.mk(null,'div','cmdline');

	var arrow=code.mk(this.cmdline,'font');
	arrow.innerHTML="&gt;";

	var form=code.mk(this.cmdline,'form');
	form.onsubmit=function(){
		var input=code.gebi('cmdline_input');
		cmd.exec(input.value);
		// exec.command(input.value);
		input.value='';
		return false;
	}

	var input=code.mk(form,'input','cmdline_input');
	input.setAttribute('type','text');
	input.focus();
},

// builds the body of the page (where the window is displayed)
buildBody: function(){
	this.body_scroll=code.mk(null,'div','body_scroll');
	var height=
		window.innerHeight-
		this.strip_bar.clientHeight-
		//parseInt(this.strip_bar.style.borderBottom)-
		2-
		this.cmdline.clientHeight-
		//parseInt(this.cmdline.style.borderBottom);
		2-
		2;
	// TEMP: hard coded borders... there has to be a solution to this!
	// TODO: border/padding around textarea for note, last "2" above is just a
	//       temp fix for this, really
	this.body_scroll.style.height=height+'px';
},

// }}}

// MENUS {{{

// builds the root menu
buildRootMenu: function(){
	core.destroyMenu();

	rootmenu.build();
	var rb_a=code.gebtn('a',0,core.root_button);
	rb_a.onclick=core.destroyRootMenu;

	menu.destroyer=core.destroyRootMenu;
},

// destroys the currently displaying menu
destroyMenu: function(){
	if(menu.destroyer) menu.destroyer();
},

// destroys the root menu
destroyRootMenu: function(){
	var rb_a=code.gebtn('a',0,core.root_button);
	rb_a.onclick=core.buildRootMenu;
	rootmenu.destroy(true);
	menu.destroyer=null;
},

// builds the local menu
buildLocalMenu: function(){
	core.destroyMenu();

	localmenu.build();
	var lb_a=code.gebtn('a',0,core.local_button);
	lb_a.onclick=core.destroyLocalMenu;

	menu.destroyer=core.destroyLocalMenu;
},

// destroys the local menu
destroyLocalMenu: function(){
	var lb_a=code.gebtn('a',0,core.local_button);
	lb_a.onclick=core.buildLocalMenu;
	localmenu.destroy(true);
	menu.destroyer=null;
},

// }}}

// WINDOWS {{{

// get window number winno
getWin: function(winno){
	return this.windows[winno];
},

// get the current window
curWin: function(){
	return this.getWin(this.selected);
},

// redraws the window bar, and displays the scroll buttons if necessary
redrawWinbar: function(){
	this.winbar_leftbtn.style.display="none";
	this.winbar_rightbtn.style.display="none";

	var i=0;
	var cur=this.winbar.firstChild;
	cur.innerHTML=i+":"+this.windows[i].title;
	var width=cur.offsetWidth;
	while(cur.nextSibling!=null){
		i++;
		cur=cur.nextSibling;
		cur.innerHTML=i+":"+this.windows[i].title;
		width+=cur.offsetWidth;
	}

	if(this.winbar.offsetLeft+width>=this.time_box.offsetLeft){
		this.winbar_leftbtn.style.display="block";
		this.winbar_rightbtn.style.display="block";
	}
},

// scrolls the window bar through intervals
scrollWinbar: function(goright){
	if(arguments.length>1){
		var offset=-20;
		if(goright) offset=20;
		this.winbar.scrollLeft+=offset;
	}
	else{
		clearInterval(this.scrollInterval);
		this.scrollInterval=setInterval(
			"core.scrollWinbar("+(goright?"true":"false")+",1);",100
		);
	}
},

// cancels the interval started by scrollWinbar
cancelScrollWinbar: function(){
	clearInterval(core.scrollInterval);
	core.scrollInterval=null;
},

// selects window winno
selWin: function(winno){
	if(this.selected==winno)
		return;

	if(this.selected!=null){
		var blk=this.curWin().winblk;
		if(blk.className=="selected"){
			blk.className="";
		}
		else{
			var cn=blk.className;
			blk.className=cn.substr(0,cn.length-9);
		}
	}
	var newblk=this.getWin(winno).winblk;
	if(newblk.className=="")
		newblk.className="selected";
	else
		newblk.className+=" selected";

	if(this.selected!=null)
		this.body_scroll.removeChild(this.curWin().blk);
	this.body_scroll.appendChild(this.getWin(winno).blk);

	this.selected=winno;
},

// adds the root window
addRootWindow: function(){
	var awin=new awindow();
	awin.mode="root";
	awin.blk=code.jmk('div','rootwin_body');

	var winno=this.windows.length;
	this.rootwin=code.mk(this.winbar,'a','rootwin');
	this.rootwin.onclick=function(){ core.selWin(winno); };

	awin.winblk=this.rootwin;
	this.windows.push(awin);
	this.selWin(0);
	this.getWin(0).setTitle("null");
},

// adds a window by their awindow object
addWindow: function(awin){
	var winno=this.windows.length;
	var winblk=code.mkc(this.winbar,'a','window');
	winblk.onclick=function(){ core.selWin(winno); };

	awin.winblk=winblk;
	this.windows.push(awin);
	this.redrawWinbar();
},

// removes a window by index number
removeWindow: function(winno){
	this.selWin(0);
	if(this.windows[winno].destroy) this.windows[winno].destroy();
	this.winbar.removeChild(this.windows[winno].winblk);
	this.windows.splice(winno,1);
},

// outputs message to the root window
rootOut: function(msg){
	if(typeof(msg)=='object')
		this.getWin(0).blk.appendChild(msg);
	else
		this.getWin(0).blk.innerHTML+=msg+"\n<br />\n";
},

// }}}

}

//vim:foldmethod=marker
