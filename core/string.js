/*
 * Conqsh - String class extensions
 *
 * Extends String to include more functionality.
 *
 * Author: Brad Cable
 * Email: brad@bcable.net
 * License: MIT
 *
 */

// trims whitespace at the beginning and end, and returns the resulting String
String.prototype.trim=function(){
	var i;
	var newobj=this;

	function isSpace(chr){
		if(chr==" " || chr=="\t" || chr=="\r" || chr=="\n")
			return true;
		return false;
	}

	for(i=0; i<newobj.length; i++){
		if(!isSpace(newobj.substr(i,1))) break;
	}

	newobj=newobj.substr(i,newobj.length);

	for(i=newobj.length-1; i>=0; i--){
		if(!isSpace(newobj.substr(i,1))) break;
	}

	return newobj;
}

// strips one layer of slashes off the String, and returns the resulting String
String.prototype.stripslashes=function(){
	var pos;
	var curstr=this;
	var newobj="";
	while(curstr.indexOf("\\")!=-1){
		pos=curstr.indexOf("\\");
		newobj+=curstr.substr(0,pos)+curstr.substr(pos+1,1);
		curstr=curstr.substr(pos+2,curstr.length);
	}
	newobj+=curstr;
	return newobj;
}

// repeats a string
String.prototype.repeat=function(l){
	return new Array(l+1).join(this);
}
