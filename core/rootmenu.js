/*
 * Conqsh - rootmenu object
 * instance of menu
 *
 * 'root' menu object controlled by the first button on the strip bar.
 *
 * Author: Brad Cable
 * Email: brad@bcable.net
 * License: MIT
 *
 */

rootmenu=new menu('root_menu');

rootmenu.menuItems=function(){
	var array=Array();

	// login stuff
	if(user.loggedIn){
		array.push(Array('logout',0,'user.doLogout();'));
		// sep
		array.push(null);

		// module listing
		for(var i=0; i<modules.installedModules.length; i++){
			array.push(Array(
				modules.installedModules[i],
				1,
				modules.installedModules[i]
			));
		}
	}
	else{
		array.push(Array('login',0,'user.doLogin();'));
		array.push(Array('create',0,'user.doCreate();'));
	}

	return array;
}
