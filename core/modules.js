/*
 * Conqsh - module object
 *
 * Handles the modules.
 *
 * Author: Brad Cable
 * Email: brad@bcable.net
 * License: MIT
 *
 */

modules={

// array of installed modules
installedModules: installedModules,

// array of loaded modules
loadedModules: Array(),

// private: current module number being loaded
current: 0,

// private: current module name being loaded
currentModuleName: null,

// load the next module on the list
loadNextModule: function(){
	if(this.current<this.installedModules.length){
		this.currentModuleName=this.installedModules[this.current];
		this.loadModule();
		this.current++;
	}
},
// loadModules and loadNextModule are technically same thing.  loadModules only
// loads all modules (like implied) if current is equal to zero.
loadModules: function(){ this.loadNextModule(); },

// load a single module
loadModule: function(){
	mod_menu=new menu('mod_menu');
	mod_include('init.js');
},

// complete the load of a module
// This function must be called from the end of the module being loaded.  The
// reason is because the 'include' and 'mod_include' functions start the
// include AFTER the script is done running, meaning just putting this code
// below mod_include above would yield weird results.
completeLoad: function(){
	eval('menu_mod_'+this.currentModuleName+'=mod_menu;');
	this.loadedModules.push(this.currentModuleName);
	this.loadNextModule();
},

}
