/*
 * Conqsh - menu class
 *
 * Describes and handles a single menu.
 *
 * Author: Brad Cable
 * Email: brad@bcable.net
 * License: MIT
 *
 */

// constructor
function menu(id){
	this.id=id;
}

// menu class
menu.prototype={

// block element of menu
blk: null,

// variable that keeps track of the parent of this menu
parent: null,

// variable that keeps track of children of this menu
children: Array(),

// array of menu items this menu is associated with
menuItems: Array(),

// builds the menu
build: function(){
	this.blk=code.mk(null,'div',this.id);
	this.blk.className="menu";

	this.blk.onmouseover=function(){
		if(menu.mouseHoverTimer)
			clearTimeout(menu.mouseHoverTimer);
	}
	this.blk.onmouseout=function(){
		menu.mouseHoverTimer=
			setTimeout('core.destroyMenu();',settings.menuMouseTimeout*1000);
	}

	if(arguments.length!=0){
		this.parent=arguments[0];
		this.parent.addChild(this);

		var menuItem=arguments[1];

		this.blk.style.left=
			(this.parent.blk.offsetLeft+
			 this.parent.blk.offsetWidth-
			 1 // hardcoded border... is there a way to fix this?
			   // this.parent.blk.style.borderRight and all variants
			   // aren't reporting the right result here
			)+'px';

		this.blk.style.top=(this.parent.blk.offsetTop+menuItem.offsetTop)+'px';
		this.blk.display='block';
	}

	var menuItems=this.menuItems;
	if(typeof(menuItems)!='object')
		menuItems=menuItems();

	for(var i=0; i<menuItems.length; i++){
		this.addItem(menuItems[i]);
	}
	return this.blk;
},

// adds a child submenu to this menu
addChild: function(child){
	this.children.push(child);
},

// destroys the menu and all children below it
destroyChildren: function(){
	for(var i=0; i<this.children.length; i++){
		this.children[i].destroy();
	}
	this.children=Array();
},

// destroys the menu
destroy: function(){
	if(this.blk){
		// if arg0 exists, then that means it came from core.destroyMenu() and
		// is unnecessary to run again
		if(
			arguments.length==0 &&
			(this==rootmenu || this==localmenu)
		) core.destroyMenu();

		this.destroyChildren();
		code.rm(this.blk);
	}
	if(menu.mouseHoverTimer) clearTimeout(menu.mouseHoverTimer);
},

// adds an item to the window
//   input format:
//    arg0 == null - separator
//    arg0 == array
//      arg0[0]      -  text to be displayed
//      arg0[1]      -  type of item (check under TYPES below)
//      arg0[2...n]  -  extra stuff to pass (check under TYPES below)
addItem: function(itemArr){

	// separator
	if(itemArr==null){
		var sep=code.mkc(this.blk,'div','menu_sep');
		return;
	}

	// regular item
	var item=code.mkc(this.blk,'div','menu_item');
	item.innerHTML=itemArr[0];

	// Default onmouseover event.  Any other onmouseover events defined here
	// must also include this code.
	var menuParent=this;
	item.onmouseover=function(){
		menuParent.destroyChildren();
	}

	// TYPES:
	switch(itemArr[1]){

	// 0 - single item, click and run
	//     arg0[2] - command to execute
	case 0:
		// click an item
		item.onclick=function(){
			rootmenu.destroy();
			eval(itemArr[2]);
		}
		break;

	// 1 - submenu for module
	//     arg0[2] - module to display submenu for
	case 1:
		// create new menu on mouseover
		item.onmouseover=function(){
			menuParent.destroyChildren();
			eval("menu_mod_"+itemArr[2]+".build(menuParent,this);");
		}
		break;
	}
},

}
