/*
 * Conqsh - command class
 *
 * Describes and handles a single command.
 *
 * Author: Brad Cable
 * Email: brad@bcable.net
 * License: MIT
 *
 */

// constructor
function command(){
	// keeps track of the handlers associated with various commands
	this.handlers=Array();

	// keeps track of mode handlers associated with various commands
	this.modeHandlers=Array();
}

// command class
command.prototype={

// register a command with this level
register: function(cmd,func){
	this.handlers[cmd.toLowerCase()]=func;
},

modeRegister: function(cmd,mode,func){
	if(!this.modeHandlers[mode])
		this.modeHandlers[mode]=Array();
	this.modeHandlers[mode][cmd.toLowerCase()]=func;
},

// parse a string into a series of arrays that represents this command
parseCmd: function(cmd){
	var extraArgs=Array();
	if(arguments.length>1) extraArgs=arguments[1];

	function oddSlashes(cmd){
		var numslashes=0;
		for(var i=cmd.length-2; i>=0; i--){
			if(cmd.substr(i,1)=="\\")
				numslashes++;
			else
				break;
		}
		return numslashes%2==1;
	}

	var cmdArr=cmd.split(';');
	var cmds=Array();
	var curCmd;
	while(curCmd=cmdArr.shift()){
		curCmd=curCmd.trim();

		// odd amounts of slashes at the end means the next stuff is still part
		// of this command
		while(oddSlashes(curCmd+";") && cmdArr.length>0){
			curCmd+=";"+cmdArr.shift();
		}

		var cmdSplit=curCmd.split(" ");
		var cmdArgs=Array();
		var curArg;
		while(curArg=cmdSplit.shift()){

			// odd amounts of slashes at the end means the next stuff is still
			// part of this command
			while(oddSlashes(curArg+" ") && cmdSplit.length>0){
				curArg+=" "+cmdSplit.shift();
			}

			// quoting means the next stuff is still part of this command
			var inQuote=null;
			if(curArg.substr(0,1)=='"' || curArg.substr(0,1)=="'"){
				inQuote=curArg.substr(0,1);
				curArg=curArg.substr(1);

				// find the next thing ending in quote, as long as an odd amount
				// of slashes does not precede it
				while(
					(curArg.substr(-1)!=inQuote || oddSlashes(curArg)) &&
					cmdSplit.length>0
				) curArg+=" "+cmdSplit.shift();

				// find the next thing ending in semicolon, as long as an odd
				// amount of slashes does not precede it
				while(
					(curArg.substr(-1)!=inQuote || oddSlashes(curArg)) &&
					cmdArr.length>0
				) curArg+=";"+cmdArr.shift();

				// take off the trailing quote, if it's there
				if(curArg.substr(-1)==inQuote)
					curArg=curArg.substr(0,curArg.length-1);
			}
			cmdArgs.push(curArg.stripslashes());
		}
		cmdArgs=cmdArgs.concat(extraArgs);
		cmds.push(cmdArgs);
	}
	return cmds;
},

// parse and run a given string based command
exec: function(cmd){
	var cmds=
		(arguments.length>1?this.parseCmd(cmd,arguments[1]):this.parseCmd(cmd));
	for(var i=0; i<cmds.length; i++){
		this.run(cmds[i]);
	}
},

// parse and run a given string based command ignoring mode handlers
// this should be identical to command.exec() with the difference that the
// second argument to command.run() should be true instead of false
baseExec: function(cmd){
	var cmds=
		(arguments.length>1?this.parseCmd(cmd,arguments[1]):this.parseCmd(cmd));
	for(var i=0; i<cmds.length; i++){
		this.run(cmds[i],true);
	}
},

// run an array as a command
run: function(cmd){
	var cmdname=(cmd[0]==undefined?'':cmd[0].toLowerCase());
	var root=(arguments.length>1?true:false);

	// handle mode handlers first
	if(
		!root &&
		core.curWin().mode &&
		this.modeHandlers[core.curWin().mode] &&
		this.modeHandlers[core.curWin().mode][cmdname]
	){
		cmd.shift();
		this.handle(this.modeHandlers[core.curWin().mode][cmdname],cmd);
	}

	// handle global handlers
	else if(this.handlers[cmdname]){
		cmd.shift();
		this.handle(this.handlers[cmdname],cmd);
	}

	// fall back on catch-all handler
	else if(this.handlers[''])
		this.handle(this.handlers[''],cmd);

	// handler not found
	else this.notfound();
},

// execute handler using cmd
handle: function(handler,cmd){

	// execute as function
	if(typeof(handler)=='function')
		handler(cmd);

	// execute as command object
	else if(typeof(handler)=='object')
		handler.run(cmd);

	// internal error
	else
		dialog.error(
			'Internal Error',
			'Unknown type for registered command.'
		);
},

// error for command not found, used by 'default' handlers
// TEMP: error needs to be changed
notfound: function(){
	dialog.error(null,'Command Not Found');
},

}
