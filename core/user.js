/*
 * Conqsh - user object
 *
 * Keeps track of and manipulates the current user.
 *
 * Author: Brad Cable
 * Email: brad@bcable.net
 * License: MIT
 *
 */

user={

// bring up the login dialog box
doLogin: function(){
	if(user.loggedIn){
		dialog.error(null,"You are already logged in.");
		return;
	}
	this.ldialog=new dialog("Conqsh Login",dialog.prototype.TYPE_OKAYCANCEL);
	this.ldialog.buttons[0]='Login';
	this.ldialog.addTextField('Username:','username');
	this.ldialog.addPassField('Password:','password');
	this.ldialog.submit=function(dialog){
		user.submitLogin(
			dialog.form.elements[0].value,
			dialog.form.elements[1].value
		);
	}
	this.ldialog.setAutoDestroy(false);
	this.ldialog.create();
	this.ldialog.show();
},

// log the user out
doLogout: function(){
	this.username=null;
	this.key=null;
	this.credentials=null;
	this.loggedIn=false;
	core.windows[0].setTitle('null');

	// loop through and destroy all windows that require login to operate
	for(var i=0; i<core.windows.length; i++){
		if(core.windows[i].loginRequired){
			core.removeWindow(i);
			i--;
		}
	}

	core.redrawWinbar();
},

// handle the input for the dialog box
submitLogin: function(username, password){
	user.key=enc.hash(password,256);
	user.credentials=enc.hex(enc.hash(password,16));
	server.httpSend({
		interface: 'user',
		action: 'getUser',
		username: username,
		credentials: "1:"+user.credentials,
	},
	function(input,out){
		if(out.status==1){
			user.username=out.username;
			user.loggedIn=true;
			user.ldialog.destroy();
			core.getWin(0).setTitle(user.username);
		}
		else if(out.error){
			user.key=null;
			user.credentials=null;
			user.ldialog.setError(out.error);
			user.ldialog.show();
		}
		else
			user.ldialog.destroy();
	});
},

}
