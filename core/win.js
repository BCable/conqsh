/*
 * Conqsh - win object
 *
 * Switches windows.
 *
 * Author: Brad Cable
 * Email: brad@bcable.net
 * License: MIT
 *
 */

win=new command();
cmd.register('win',alias);

alias.register('',function(args){
	if(args[0]){
		if(args[0]==parseInt(args[0])) core.selWin(args[0]);
		else dialog.error(null,"Invalid syntax");
	}
	// TEMP: get this on root window error
	else dialog.error(null,"Unspecified window");
});
