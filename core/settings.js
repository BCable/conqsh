/*
 * Conqsh - settings object
 *
 * Keeps track of the user's settings.
 *
 * Author: Brad Cable
 * Email: brad@bcable.net
 * License: MIT
 *
 */

settings={

// when you mouse out of a menu, this is the time it takes for all menus to be
// timed out and destroyed
menuMouseTimeout: 2,

// the format used for the time display
//timeFormat: "l F j, Y - g:i:s A",
timeFormat: "D j G:i",

}
