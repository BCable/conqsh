/*
 * Conqsh - alias object
 *
 * Retrieves and stores aliases for the cmd object.
 *
 * Author: Brad Cable
 * Email: brad@bcable.net
 * License: MIT
 *
 */

// TODO: Get this to store information on the server

alias=new command();
cmd.register('alias',alias);

alias.aliases=Array();

alias.register('',function(args){
	if(args[0]){
		alias.aliases[args[0]]=args[1];
		// TODO: output success and a visible version of the alias in the root
		// window
	}
	else{
		// TEMP: lists aliases, but for now can only do so through alerts
		var str='';
		for(var i in alias.aliases){
			str+=i+": "+alias.aliases[i];
		}
		alert(str);
	}
});

// working request!
cmd.register('a',function(args){
	server.httpSend({
		module: 'note',
		action: 'getNote',
		name: 'note',
	},
	function(input,out){
		if(out.status==1){
			alert(out.body);
		}
	}
	);
});
