/*
 * Conqsh - time object and Date class extending
 *
 * Keeps track of and calculates stuff with the time display.  The Date class
 * extensions are there to help in this regard.
 *
 * Author: Brad Cable
 * Email: brad@bcable.net
 * License: MIT
 *
 */

// Date class extensions

// gets text based days array
Date.prototype.getDaysArray=function(){ return new Array(
	"Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"
); }

// gets the text representation of this object's day
Date.prototype.getTextDay=function(){
	return this.getDaysArray()[this.getDay()];
}

// gets text based months array
Date.prototype.getMonthsArray=function(){ return new Array(
	"January","February","March","April","May","June","July","August",
	"September","October","November","December"
); }

// gets the text representation of this object's month
Date.prototype.getTextMonth=function(){
	return this.getMonthsArray()[this.getMonth()];
}

// returns a boolean answer to "is this object currently in a leap year"
Date.prototype.getLeapYear=function(){
	var year=this.getFullYear();
	return ((year%100==0 && year%400==0) || (year%100!=0 && year%4==0)?1:0);
}

// gets the meridiem string (AM/PM)
Date.prototype.getMeridiem=function(){
	return (this.getHours()<=11?"A":"P")+"M";
}

// gets the number of days in the current month
Date.prototype.getDaysInMonth=function(){
	var month=this.getMonth();
	return
		(month==1?
		 28+this.getLeapYear():
		 (month%2==0?(month<7?31:30):(month<7?30:31))
		);
}


// time object
time={

// starts the time handling on specified block element
handle: function(blk){
	this.blk=blk;
	setInterval('time.redraw();',1000);
	time.redraw();
},

// redraws the block element associated with this object
redraw: function(){
	this.blk.innerHTML=this.formatDate(new Date(), settings.timeFormat);
},

// returns a string with a minimum of two digits in it
twoDigits: function(str){
	str=str.toString();
	return (str.length<2?"0":"")+str;
},

// equivalent of PHP's date()
formatDate: function(dateo,timeFormat){
	var fd="";
	for(var i=0; i<timeFormat.length; i++){
		switch(timeFormat.substr(i,1)){
			// Everything commented are options available in PHP's date()
			// function, but not in this (obviously).  For each, I'm either too
			// lazy to code them or haven't really looked into it yet.

			// Day \\

			case "d": fd+=this.twoDigits(dateo.getDate()); break;
			case "D": fd+=dateo.getTextDay().substr(0,3); break;
			case "j": fd+=dateo.getDate(); break;
			case "l": fd+=dateo.getTextDay(); break;
			case "N": fd+=dateo.getDay()+1; break;
			//case "S": fd+=dateo.getDay(); break;
			case "w": fd+=dateo.getDay(); break;
			//case "z": fd+=dateo.getDay(); break;


			// Week \\

			//case "W": fd+=dateo.getDay(); break;


			// Month \\

			case "F": fd+=dateo.getTextMonth(); break;
			case "m": fd+=this.twoDigits(dateo.getMonth()+1); break;
			case "M": fd+=dateo.getTextMonth().substr(0,3); break;
			case "n": fd+=dateo.getMonth()+1; break;
			//case "t": fd+=dateo.getMonth(); break;


			// Year \\

			case "L": fd+=dateo.getLeapYear(); break;
			//case "o": fd+=dateo.getMonth(); break;
			case "Y": fd+=dateo.getFullYear(); break;
			case "y": fd+=dateo.getFullYear().substr(2,2); break;


			// Time \\

			case "a": fd+=dateo.getMeridiem().toLowerCase(); break;
			case "A": fd+=dateo.getMeridiem(); break;
			//case "B": fd+=dateo.getMonth(); break;
			case "g": fd+=dateo.getMonth()+1; break;
			case "G": fd+=dateo.getHours(); break;
			case "h": fd+=this.twoDigits(dateo.getMonth()+1); break;
			case "H": fd+=this.twoDigits(dateo.getHours()); break;
			case "i": fd+=this.twoDigits(dateo.getMinutes()); break;
			case "s": fd+=this.twoDigits(dateo.getSeconds()); break;


			// if it's not here, then just add that character
			default: fd+=timeFormat.substr(i,1); break;
		}
	}
	fd=fd.replace(/ /g,"&nbsp;");
	return fd;
},

}
