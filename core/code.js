/*
 * Conqsh - code object
 *
 * Provides code shortcuts for the app.
 *
 * Author: Brad Cable
 * Email: brad@bcable.net
 * License: MIT
 *
 */

code={

// gets an element by tag name
gebtn: function(tagname){
	var index=0;
	var parent=document;
	if(arguments.length>1) index=arguments[1];
	if(arguments.length>2) parent=arguments[2];
	return parent.getElementsByTagName(tagname)[index];
},

// gets an element by id
gebi: function(id){
	var parent=document;
	if(arguments.length>1) parent=arguments[1];
	return parent.getElementById(id);
},

// just creates a new element
dce: function(tagname){
	return document.createElement(tagname);
},

// removes an element
rm: function(elem){
	if(elem.parentNode)
		return elem.parentNode.removeChild(elem);
},

// makes a new element, with parent and id passed in
mk: function(parent, tagname){
	if(parent==null) parent=code.gebtn('body');
	var elem=this.dce(tagname);
	if(arguments.length>2) elem.setAttribute('id',arguments[2]);
	parent.appendChild(elem);
	return elem;
},

// makes a new element, with parent and class passed in
mkc: function(parent, tagname){
	if(parent==null) parent=code.gebtn('body');
	var elem=this.dce(tagname);
	if(arguments.length>2) elem.setAttribute('class',arguments[2]);
	parent.appendChild(elem);
	return elem;
},

// just makes a new element, with id passed in (not appended to anything yet)
jmk: function(tagname){
	var elem=this.dce(tagname);
	if(arguments.length>1) elem.setAttribute('id',arguments[1]);
	return elem;
},

// just makes a new element, with class passed in (not appended to anything yet)
jmkc: function(tagname){
	var elem=this.dce(tagname);
	if(arguments.length>1) elem.setAttribute('class',arguments[1]);
	return elem;
},

// returns a non-pointer copy of an object
ocpy: function(obj){
	var ret={};
	for(var attr in obj) ret[attr]=obj[attr];
	return ret;
},

}
