/*
 * Conqsh - server object
 *
 * Retrieves and stores data from the server.
 *
 * Author: Brad Cable
 * Email: brad@bcable.net
 * License: MIT
 *
 */

server={

// keeps track of the current XMLHttpRequest objects
xmlhttp: new Array(),

// keeps track of the response handlers
handlers: new Array(),

// keeps track of the input data given so that the handler can receive that
// information
inputs: new Array(),

// creates a new XMLHttpRequest object
newhttp: function(){
	core.loading();
	var idx=this.xmlhttp.length;
	var msiexml=["Msxml2.XMLHTTP","Microsoft.XMLHTTP","Msxml2.XMLHTTP.4.0"];
	this.xmlhttp[idx]=null;
	try{
		this.xmlhttp[idx]=new XMLHttpRequest();
		this.xmlhttp[idx].idx=idx;
	}
	catch(e){
		for(var i=0; i<msiexml.length; i++){
			try{ this.xmlhttp[idx]=new ActiveXObject(msiexml[i]); }
			catch(f){ this.connectionFailed(idx); }
		}
	}
	return this.xmlhttp[idx];
},

// notifies the user that the connection failed
connectionFailed: function(idx){
	dialog.error("Cannot connect to server.",
		"Please check your network connection and try again.  If that doesn't "+
		"work, try refreshing the page."
	);

	if(idx!=undefined) this.xmlhttp[idx]=null;
},

// the eventhandler for a XMLHttpRequest
eventhandler: function(){
	var xmlhttp=server.xmlhttp[this.idx];
	if(xmlhttp.readyState==4){
		if(xmlhttp.status==200){

			// parse output
			var text=xmlhttp.responseText;
			var arr=text.split('&');
			var out={};
			for(var i=0; i<arr.length; i++){
				var key=arr[i].substr(0,arr[i].indexOf('='));
				var val=arr[i].substr(arr[i].indexOf('=')+1,arr[i].length);

				// multidimensional array parsing
				var rightIdx;
				var leftIdx=key.indexOf('[');
				if(
					leftIdx!=-1 &&
					out[key.substr(0,key.indexOf('['))]==undefined
				) out[key.substr(0,key.indexOf('['))]=new Array();

				while(leftIdx!=-1){
					rightIdx=key.indexOf(']',leftIdx);
					console.log(
						'if(out.'+key.substr(0,rightIdx+1)+'==undefined) '+
						'out.'+key.substr(0,rightIdx+1)+'=new Array();'
					);
					eval(
						'if(out.'+key.substr(0,rightIdx+1)+'==undefined) '+
						'out.'+key.substr(0,rightIdx+1)+'=new Array();'
					);
					leftIdx=key.indexOf('[',leftIdx+1);
				}
				if(rightIdx!=undefined){
					console.log('out.'+key+'=val;');
					eval('out.'+key+'=val;');
				}

				// wasn't a multidimensional array, just set the value normally
				else out[key]=val;

			}
			if(!out.status) out.status=1;

			// server error
			if(out.status==-1){
				core.rootOut(
					'<b>INTERNAL PHP ERROR ('+
					out.errno+
					'):</b> '+
					out.errmsg+
					'<b> in file </b>'+
					out.filename+
					'<b> on line </b>'+
					out.linenum
				);
			}

			// send prior input and parsed output to handler and quit
			server.handlers[this.idx](server.inputs[this.idx],out);
			server.xmlhttp[this.idx]=null;

		}
		else server.connectionFailed(this.idx);
		core.done();
	}
},

// the way to send an HTTP request through the app
httpSend: function(data,handler){

	var input=data;

	// tack on user credentials if the request is a module request
	if(data['module']!=undefined){
		if(!user.loggedIn)
			return false;
		data['conqsh_username']=user.username;
		data['conqsh_credentials']="1:"+user.credentials;
	}

	// build request
	var request='';
	for(var i in data){
		request+='&'+i+'='+data[i];
	}
	request=request.substr(1);

	// create the thing
	var xmlhttp=this.newhttp();
	xmlhttp.open("POST",server_interface,true);

	// event handler
	var eventhandler=this.eventhandler;
	eventhandler.idx=xmlhttp.idx;
	xmlhttp.onreadystatechange=eventhandler;

	// send headers and the request
	xmlhttp.setRequestHeader(
		"Content-Type",
		"application/x-www-form-urlencoded"
	);
	xmlhttp.send(request);

	// store handler and input data
	this.handlers[xmlhttp.idx]=handler;
	this.inputs[xmlhttp.idx]=input;

	return true;

},

}
