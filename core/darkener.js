/*
 * Conqsh - darkener object
 *
 * Displays a dark background with opacity over everything under z-index 10.
 *
 * Author: Brad Cable
 * Email: brad@bcable.net
 * License: MIT
 *
 */

darkener={

// on script init, create the block element
init: function(){
	this.blk=code.mk(null,'div','darkener');
},

// show the darkener
show: function(){
	this.blk.style.display='block';
},

// hide the darkener
hide: function(){
	this.blk.style.display='none';
},

}
