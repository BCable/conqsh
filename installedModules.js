/*
 * Conqsh - installedModules array
 *
 * Keeps track of the current installed modules.
 *
 * Author: Brad Cable
 * Email: brad@bcable.net
 * License: Modified BSD
 * License Details:
 * http://bcable.net/license.php
 *
 */

installedModules=Array(
'note'
);
