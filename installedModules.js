/*
 * Conqsh - installedModules array
 *
 * Keeps track of the current installed modules.
 *
 * Author: Brad Cable
 * Email: brad@bcable.net
 * License: MIT
 *
 */

installedModules=Array(
'note'
);
