/*
 * Conqsh - note module - win.js
 *
 * Extends the awindow class for the note module.
 *
 * Author: Brad Cable
 * Email: brad@bcable.net
 * License: MIT
 *
 */

note_win=awindow.prototype.new();
note_win.prototype.setMode("note");
note_win.prototype.setTitle("note");
note_win.prototype.setLoginRequired(true);

note_win=function(text){
	var nw=new awindow();
	nw.blk=code.jmkc('textarea','note_textarea');
	nw.blk.value=text;
	nw.blk.ivalue=text;
	nw.blk.onkeydown=function(){
		setTimeout('note_win.prototype.onkeydowner();',10);
	}
	return nw;
}

note_win.prototype.onkeydowner=function(){
	if(core.curWin().blk.ivalue!=core.curWin().blk.value){
		core.curWin().setTitle(core.curWin().title+"*");
		core.curWin().blk.onkeydown=null;
	}
}
