<?
/*
 * Conqsh - note module installation
 *
 * Creates the database table 'note'.
 *
 * Author: Brad Cable
 * Email: brad@bcable.net
 * License: MIT
 *
 */

$secdb->query_exec('drop table if exists note');
$secdb->query_exec(
'create table note(
	note_id int(7),
	parent_id int(7),
	name text,
	body text
)');

?>
