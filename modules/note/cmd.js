/*
 * Conqsh - note module - command.js
 *
 * Creates and registers the commands for the module.
 *
 * Author: Brad Cable
 * Email: brad@bcable.net
 * License: MIT
 *
 */

note_cmd=new command();
cmd.register('note',note_cmd);

note_cmd.register('',function(args){
	if(args.length==0){
		server.httpSend({
			module: 'note',
			action: 'listNotes',
			name: '',
		},
		function(input,out){
			if(out.status==1){
			core.rootOut('Note Listing:');
				for(var i=0; i<out.notes.length; i++){
					core.rootOut(out.notes[i]['name']);
				}
			}
		});
	}

	else{
		server.httpSend({
			module: 'note',
			action: 'getNote',
			name: args.join(' '),
		},
		function(input,out){
			if(out.status==1 || out.status==3){
				var data;
				if(
					out.status==1 &&
					(out['n1.name']!=undefined || out['name']!=undefined)
				){
					data=out;
					if(data['name']==undefined){
						data.name=data['n1.name'];
						data.body=data['n1.body'];
						data.note_id=data['n1.note_id'];
					}
				}
				else if(out.status==3){
					data={};
					data.name=input.name;
					data.body='';
				}

				var newwin=new note_win(data.body);
				newwin.setTitle("note:"+data.name);
				core.addWindow(newwin);
				core.selWin(core.windows.length-1);
			}
			// TEMP: make this a root window dump
			else dialog.error(null,out.error);
		});
	}
});

cmd.modeRegister('close','note',function(args){
	cmd.baseExec('close');
	alert('note mode\'s close function');
});
