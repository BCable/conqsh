/*
 * Conqsh - note module
 *
 * Allows the user to enter and manage notes.
 *
 * Author: Brad Cable
 * Email: brad@bcable.net
 * License: MIT
 *
 */

mod_include('menu.js');
mod_include('cmd.js');
mod_include('win.js');
mod_cinclude('main.css');
mod_end();
