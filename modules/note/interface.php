<?
/*
 * Conqsh - note module interface
 *
 * Interfaces with the database for the module 'note'.
 *
 * Author: Brad Cable
 * Email: brad@bcable.net
 * License: MIT
 *
 */

class mod_note{
	var $loginRequired=true;

	function getNote($post){
		global $secdb;

		// construct query
		if(strpos($post['name'],' ')===false){
			$query='select * from note as n1 where name===s==';
			$qryargs=$post['name'];
		}
		else{
			$query='select n1.* from note as n1';
			$qryarr=explode(' ',$post['name']);
			$qrywhr=null;
			$qryargs=array();
			for($i=0; $i<count($qryarr)-1; $i++){
				$query.=
					' left join note as n'.($i+2).
					' on n'.($i+1).'.parent_id=n'.($i+2).'.note_id';
				$qrywhr.=' and n'.($i+2).'.name===s==';
				$qryargs[]=$qryarr[$i];
			}
			$qryargs[]=$qryarr[$i];
			$qryargs=array_reverse($qryargs);
			$query.=" where n1.name===s=={$qrywhr}";
		}

		$array=$secdb->query_onerow(
			userQuery($query,'n1'),
			$qryargs
		);

		if(is_array($array))
			return $array;
		else
			return array(
				'status'=>3,
				'error'=>'Invalid note.'
			);
	}

	function saveNote($post){
		global $secdb;
		$note_id=$secdb->query_onefield(
			userQuery('select note_id from note where note_id===i=='),
			$post['note_id']
		);

		if(!empty($note_id)){
			$secdb->query_exec(
				userQuery('update users set body===s== where note_id===i=='),
				$post['body'],
				$post['note_id']
			);
		}

		else{
			$secdb->query_exec(
				'insert into note (user_id, parent_id, name, body) values '.
				'(==i==,==i==,==i==,==s==,==s==)',
				$post['user_id'],
				$post['parent_id'],
				$post['name'],
				$post['body']
			);
		}
	}

	function listNotes($post){
		global $secdb;
		$ret=array();

		if($post['name']!=null){
			$ret=$secdb->query(
				'select * from note as n
				left join note as p on n.parent_id=p.note_id
				where p.name===i=='
			);
		}
		else{
			// TODO: SqLite
			$secdb->query('select * from note where parent_id isnull');
			// TODO: MySQL
			#$secdb->query('select * from note where isnull(parent_id)');

			$array=array();
			while($row=$secdb->fetch_assoc()){
				$array[]=$row;
			}

			$ret['notes']=$array;
		}
		return $ret;
	}
}

$mod_interface=new mod_note();

?>
