/*
 * Conqsh - note module - menu.js
 *
 * Creates the menu for the note module.
 *
 * Author: Brad Cable
 * Email: brad@bcable.net
 * License: MIT
 *
 */

mod_menu.menuItems=Array(
	Array('item1',0,'item1();'),
	Array('item2',0,'item2();'),
	Array('item3',0,'item3();'),
	Array('item4',0,'item4();')
);
