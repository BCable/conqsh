<?
/*
 * Conqsh - interface
 *
 * Handles incoming requests for data from Javascript.
 *
 * Author: Brad Cable
 * Email: brad@bcable.net
 * License: MIT
 *
 */

include('secdb.php');

define('REG_CLEANER','/[^a-z0-9\_\.\-~]/i');

// parse and output response
function outArr($outarr,$prefix=null){
	$first=true;
	while(list($key,$val)=each($outarr)){
		if($first) $first=false;
		else echo '&';

		if(is_array($val)){
			if($prefix!=null)
				$newpref="{$prefix}['{$key}']";
			else $newpref=$key;
			outArr($val,$newpref);
		}
		else{
			if($prefix!=null)
				echo "{$prefix}['{$key}']={$val}";
			else echo "{$key}={$val}";
		}
	}
}

// errors are handled by script
error_reporting(0);

// error handling function
function errorHandler($errno, $errmsg, $filename, $linenum, $vars){
	$ret=array();
	$ret['status']=-1;
	$ret['errno']=$errno;
	$ret['errmsg']=$errmsg;
	$ret['filename']=$filename;
	$ret['linenum']=$linenum;
	#$ret['vars']=$vars; # probably not a good idea
	outArr($ret);
	exit();
}

// register error handler
set_error_handler('errorHandler');

// action input
if(empty($_POST['action'])) exit('Error: No Action Given');
else{
	$act=$_POST['action'];
	$act=preg_replace(REG_CLEANER,null,$act);
}

// module input
if(!empty($_POST['module'])){
	$mod=$_POST['module'];
	$mod=preg_replace(REG_CLEANER,null,$mod);
}

// interface input
if(!empty($_POST['interface'])){
	$interface=$_POST['interface'];
	$interface=preg_replace(REG_CLEANER,null,$interface);
}

// check to see if there's an interface to work with
if(empty($mod) && empty($interface))
	exit('Error: No Module or Core Interface Given');

// executes the action and outputs the data
function doAction($if,$act){
	global $_POST;

	if(!method_exists($if,$act))
		exit('Error: Action doesn\'t exist.');

	// call action being referenced
	$outarr=$if->$act($_POST);

	outArr($outarr);
}

// core interface
if(!empty($interface)){
	if(file_exists("{$interface}_interface.php")){
		include("{$interface}_interface.php");
		doAction($core_interface,$act);
	}
	else exit('Error: Core Interface Not Found');
}

// module interface
elseif(!empty($mod) && file_exists("../modules/{$mod}/interface.php")){
	include("../modules/{$mod}/interface.php");

	// see if login is required for this module
	if($mod_interface->loginRequired){

		// check to see if credentials were sent
		if(
			empty($_POST['conqsh_credentials']) ||
			empty($_POST['conqsh_username'])
		){
			outArr(array(
				'status'=>2,
				'error'=>'You are not logged in.'
			));
			exit();
		}
		
		// verify the user's credentials
		$user_id=false;
		$user=$secdb->query_onerow(
			'select user_id, credentials from users where username===s==',
			$_POST['conqsh_username']
		);
		if($user['credentials']==$_POST['conqsh_credentials'])
			$user_id=$user['user_id'];

		// exit if not correct credentials
		if($user_id===false){
			outArr(array(
				'status'=>2,
				'error'=>'Invalid credentials.'
			));
			exit();
		}

		function userQuery($query,$table=null){
			global $user_id;
			if($table==null)
				return "{$query} and user_id={$user_id}";
			else
				return "{$query} and {$table}.user_id={$user_id}";
		}

	}

	doAction($mod_interface,$act);
}

// error
else exit('Error: Module not found.');

?>
