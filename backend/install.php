<?
/*
 * Conqsh - installation of database tables
 *
 * Creates the database tables 'users' and 'settings'.
 *
 * Author: Brad Cable
 * Email: brad@bcable.net
 * License: MIT
 *
 */

include('secdb.php');

// users table
$secdb->query_exec('drop table users');
$secdb->query_exec(
'create table users(
	user_id int(5),
	username text,
	encryption_enabled int(1),
	lastaction int(11)
)');

// settings table
$secdb->query_exec('drop table settings');
$secdb->query_exec(
'create table settings(
	name text,
	modulename text,
	value text
)');

?>
