<?
/*
 * Conqsh - user interface
 *
 * Interfaces with the database for the user stuff.
 *
 * Author: Brad Cable
 * Email: brad@bcable.net
 * License: MIT
 *
 */

class core_user{
	function getUser($post){
		global $secdb;
		$array=$secdb->query_onerow(
			'select * from users where username===s==',
			$post['username']
		);
		if(is_array($array))
			if($array['credentials']==$post['credentials'])
				return $array;
			else
				return array(
					'status'=>3,
					'error'=>'Invalid username or password.'
				);
		else
			return array('status'=>2, 'error'=>'Username Not Found');
	}
}

$core_interface=new core_user();

?>
